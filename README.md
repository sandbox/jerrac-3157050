# Media File Import Wizard
This module allows you to import multiple Media files at once,
and then presents you with a wizard to process the files.

For example, you could import a folder of images, then when
the wizard dialog starts, you can create entities for each
image, add the image to existing entities, or just leave
them as media library items.

## Usage
* Upload a folder of files to your private files directory.
* Go to `media/import_wizard`.
* Enter the path to the folder in the dialog.
* After the files are scanned, select the actions you wish to take for
each file.

## Credits
* For getting me started on multistep forms: https://www.sitepoint.com/how-to-build-multi-step-forms-in-drupal-8/
