<?php

namespace Drupal\media_file_import_wizard\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for media_file_import_wizard routes.
 */
class MediaFileImportWizardController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
