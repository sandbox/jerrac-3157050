<?php

namespace Drupal\media_file_import_wizard\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure media_file_import_wizard settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_file_import_wizard_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['media_file_import_wizard.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['default_source_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Source Folder'),
      '#default_value' => $this->config('media_file_import_wizard.settings')->get('default_source_folder'),
      '#description' => $this->t('Set the folder that the wizard checks if the user does not change anything.')
    ];
    $form['default_public_import_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Public Import Folder'),
      '#default_value' => $this->config('media_file_import_wizard.settings')->get('default_public_import_folder'),
      '#description' => $this->t('Set the folder that you import public files to. You can override this per file if needed.')
    ];
    $form['default_private_import_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Private Import Folder'),
      '#default_value' => $this->config('media_file_import_wizard.settings')->get('default_private_import_folder'),
      '#description' => $this->t('Set the folder that you import private files to. You can override this per file if needed.')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $validPathRegex = '$([A-z0-9-_+]+\/)*([A-z0-9]+)$';
    $default_source_folder_value = $form_state->getValue('default_source_folder');
    if(!preg_match($validPathRegex,$default_source_folder_value)){
      $form_state->setErrorByName('default_source_folder', $this->t('The Default Source Folder value is not a valid path.'));
    }
    $default_public_import_folder_value = $form_state->getValue('default_public_import_folder');
    if(!preg_match($validPathRegex,$default_public_import_folder_value)){
      $form_state->setErrorByName('default_public_import_folder', $this->t('The Default Public Import Folder value is not a valid path.'));
    }
    $default_private_import_folder_value = $form_state->getValue('default_private_import_folder');
    if(!preg_match($validPathRegex,$default_private_import_folder_value)){
      $form_state->setErrorByName('default_private_import_folder', $this->t('The Default Private Import Folder value is not a valid path.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('media_file_import_wizard.settings')
      ->set('default_source_folder', $form_state->getValue('default_source_folder'))
      ->save();
    $this->config('media_file_import_wizard.settings')
      ->set('default_public_import_folder', $form_state->getValue('default_public_import_folder'))
      ->save();
    $this->config('media_file_import_wizard.settings')
      ->set('default_private_import_folder', $form_state->getValue('default_private_import_folder'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
