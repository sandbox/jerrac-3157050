<?php
/**
 * @file
 * Contains \Drupal\media_file_import_wizard\Form\Multistep\MultistepTwoForm.
 */

namespace Drupal\media_file_import_wizard\Form\Multistep;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Url;
use Drupal\user\PrivateTempStoreFactory;

class ImportWizardTwo extends MultistepFormBase {

  public $files;


  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    parent::__construct($temp_store_factory, $session_manager, $current_user);
    $this->files = [];
    // Check if source folder exists.
    $source_folder = 'private://' . $this->store->get('source_folder');
    /* @var \Drupal\Core\File\FileSystem $file_system */
    $file_system = \Drupal::service('file_system');
    try {
      $scanned_files = $file_system->scanDirectory($source_folder,'^.*^');
      foreach($scanned_files as $file){
        $mime_type = mime_content_type($file_system->realpath($file->uri));
        $file_size = filesize($file_system->realpath($file->uri));
        $file_data = [
          'uri' => $file->uri,
          'name' => $file->filename,
          'mime_type' => $mime_type,
          'size' => $file_size, // In bytes.
        ];
        $this->logger->warning('Mime: @mime File data: @data',['@data'=>print_r($file_data,true)]);
        $this->files[] = $file_data;
      }
    } catch (Drupal\Core\File\Exception\NotRegularDirectoryException $e) {
      $this->logger->error('Source folder @source_dir does not appear to exist. Drupal threw an exeption: @exception.', [
        '@source_dir' => $source_folder,
        '@exception' => $e->getMessage(),
      ]);
    }


  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'import_wizard_two';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['file_list'] = [
      '#type' => 'table',
      '#title' => t('File List'),
      '#header' => [
        'file_name' => t('Name'),
        'file_type' => t('Type'),
        'file_size' => t('Size'),
        'public_import_folder' => t('Public Import Folder'),
        'private_import_folder' => t('Private Import Folder'),
        'import_to_public_or_private' => t('Import to')
      ]
    ];

    foreach($this->files as $file){
      $row_id = file_munge_filename($file['name'],'txt png');
      $form['file_list'][$row_id] = [
        'file_name' => ['#type'=>'html_tag','#tag' => 'span', '#value'=>$file['name'],],
        'file_type' => ['#type'=>'html_tag','#tag' => 'span', '#value'=>$file['mime_type'],],
        'file_size' => ['#type'=>'html_tag','#tag' => 'span', '#value'=>$file['size'],],
        'public_import_folder' => [
          '#type' => 'textfield',
          '#title' => t('Public Import Folder'),
          '#title_display' => 'invisible',
          '#default_value' => $this->store->get('public_import_folder'),
          '#size' => 30,
          '#field_prefix' => 'public://'
          ],
        'private_import_folder' => [
          '#type' => 'textfield',
          '#title' => t('Private Import Folder'),
          '#title_display' => 'invisible',
          '#default_value' => $this->store->get('private_import_folder'),
          '#size' => 30,
          '#field_prefix' => 'private://'
        ],
        'import_to_public_or_private' => [
          '#type' => 'radios',
          '#title' => t('Import to public or private'),
          '#title_display' => 'invisible',
          '#options' => [
            0 => t('Public'),
            1 => t('Private')
          ],
          '#default_value' => 1

        ]
      ];
    }

    $form['actions']['previous'] = [
      '#type' => 'link',
      '#title' => $this->t('Previous'),
      '#attributes' => [
        'class' => ['button'],
      ],
      '#weight' => 0,
      '#url' => Url::fromRoute('media_file_import_wizard.import_wizard_start'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('source_folder', $form_state->getValue('source_folder'));
//    $this->store->set('location', $form_state->getValue('location'));

    // Save the data
    parent::saveData();
    $form_state->setRedirect('media_file_import_wizard.import_wizard_two');
  }

}
