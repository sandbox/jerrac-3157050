<?php
/**
 * @file
 * Contains \Drupal\media_file_import_wizard\Form\Multistep\MultistepOneForm.
 */

namespace Drupal\media_file_import_wizard\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;

class ImportWizardStart extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'import_wizard_start';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['source_folder'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Source Folder'),
      '#default_value' => $this->config('media_file_import_wizard.settings')->get('default_source_folder') ? $this->config('media_file_import_wizard.settings')->get('default_source_folder') : '',
      '#description' => $this->t('Upload a folder to your private data location, then enter the relative folder path here.')
      );
    $form['public_import_folder'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Public Import Folder'),
      '#default_value' => $this->config('media_file_import_wizard.settings')->get('default_public_import_folder') ? $this->config('media_file_import_wizard.settings')->get('default_public_import_folder') : '',
      '#description' => $this->t('The publicly accessible folder you wish imported files to be saved into. You can override on a per file basis if needed later in the Wizard.')
    );
    $form['private_import_folder'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Private Import Folder'),
      '#default_value' => $this->config('media_file_import_wizard.settings')->get('default_private_import_folder') ? $this->config('media_file_import_wizard.settings')->get('default_private_import_folder') : '',
      '#description' => $this->t('The privately accessible folder you wish imported files to be saved into. You can override on a per file basis if needed later in the Wizard.')
    );

    $form['actions']['submit']['#value'] = $this->t('Next');
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $validPathRegex = '$([A-z0-9-_+]+\/)*([A-z0-9]+)$';
    $source_folder_value = $form_state->getValue('source_folder');
    if(!preg_match($validPathRegex,$source_folder_value)){
      $form_state->setErrorByName('source_folder', $this->t('The Source Folder value is not a valid path.'));
    }
    $public_import_folder_value = $form_state->getValue('public_import_folder');
    if(!preg_match($validPathRegex,$public_import_folder_value)){
      $form_state->setErrorByName('public_import_folder', $this->t('The Public Import Folder value is not a valid path.'));
    }
    $private_import_folder_value = $form_state->getValue('private_import_folder');
    if(!preg_match($validPathRegex,$private_import_folder_value)){
      $form_state->setErrorByName('private_import_folder', $this->t('The Private Import Folder value is not a valid path.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('source_folder', $form_state->getValue('source_folder'));
    $this->store->set('public_import_folder', $form_state->getValue('public_import_folder'));
    $this->store->set('private_import_folder', $form_state->getValue('private_import_folder'));
    $form_state->setRedirect('media_file_import_wizard.import_wizard_two');
  }
}
